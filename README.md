# Cryptography Fundamentals


Cryptography enables pretty much all online activiey in today's age. SSL (TSL) enables confidence in the sites we visit, asymmetric keys let us identify ourselves without risk of imposters, hashing allows us to trust the content we recieve, and symmetric keys allow private communication to occur over public channels.

## Hashing

Hashing is a means to prepare a "fingerprint" of a message. A message is put to an algorythm to generate the hash of the message. Any modification to the message will change the output hash. Thus, if 2 messages generate the same hash, then both messages are the exact same.

It is possible for 2 different messages to generate the same hash (hash collision), but it is in theory difficult to prepare 2 such different messages on purpose.




## Symmetric Key Encryption/Decryption

Symmetric key encryption is fast, desirable for rapid communication

- one key to encrypt and decrypt
- requisite for encrypted communication for the sake of speed/computing energy
- both parties know the key, and nobody else can know them

Symmetric keys are used to robustly communicate, but how do 2 parties agree on the key without anybody else being privy?

## Asymmetric Key Encryption/Decryption

Asymmetric Key Encryption creates 2 keys: 1 that is shared freely (public key) and 1 that is kept secret (private key)
- only the public key can decrypt a message encrypted with the private key
- only the private key can decrypt a message ecrypted with the public key

### Signatures

I encrypt a message with my private key. If you are able to decrypt with my public key, it is then known that I and only I could have generated the enrypted message in the first place. It is now known that I was the source of the encrypted message.

### Secret Messages

I encrypt a message with your public key. I can be sure that only you can decrypt it since only you have the proper key to decrypt the message.

### Internet Certifications, Certificate Authorities

We all trust Google (!?) to say who they are and to vouce for others. We also trust Let's Encrypt. If they say someone is who they say they are, we will trust them.

Thus, Let's Encrypt will use their private key to sign a websites public key. I will trust that, since I have Let's Encrypt's public key and I can use it to decrypt a message that contains a websites public key, I will trust that only Let's Encrypt could have generated the encypted key, and I will believe that it is authentic since Let's Encrypt is **trusted** to me. Thus I trust the public key and if a message that comes to me can be decrypted with this public key, I will trust that the message originated from the source that Let's Encrypt says it is.

Breakdown
- A website gives Let's Encrypt their public key
- Let's Encrypt encrypts the website's public key, domain information, and other information with their private key, called a certificate
- When I visit the website, 
- My browser and the server agree what encryption methods we will use to authenticate and communicate 

my browser requests the certificate
- I decrypt it with Let's Encrypt's public key that I trust, thus trust the websites public key that I found
- I send the website a symmetric key encrypted with their confirmed public key
- The website responds 
- I send them an symmetric key that we will use from now on (of course encrypted with their public key that they can decrypt and see)
- Now we have an agree'd upon symmmetric key that only we know about and we can encrypt our communications with it for now

### Cryptographic Signatures

I know your public key. When you give a message encrypted by your private key and I can decode it with your public key, I know it could have only come from you.

Breakdown
- You generate a message, then encrypt it with your private key.
- I recieve the (encrypted) message and successfuly decrypt it with your public key.
- Since I was successful to decrypt it, I know the message must have come from you (and your private key)


## Takeaways

- It is important to understand the secrecy around the keys, what is the nature of the keys, and who knows what information
- If you have a public key, then you better be the only one with the private key: all that hold the private key can masquarade as you
- symmetric keys, while convenient, are disposable. We will nogotiate them for this session then toss them, create new ones next time we talk


